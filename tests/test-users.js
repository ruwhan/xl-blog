process.env.NODE_ENV = 'test';

var mongoose = require('mongoose'),
    mockgoose = require('mockgoose'),
    express = require('express'),    
    request = require('supertest'),
    chai = require('chai'),
    should = chai.should(),
    config = require('../config/config'),
    User = require('../app/models/user'),
    repository = require('../app/data/user-repository').Repository,
    app = express();

require('../config/express')(app, config);
require('../config/routes')(app);

describe("testing user", function() {

    var db = mongoose.connection;

    before(function(done) {
        db.open(config.db, function(err) {
            if (err) {
                return done(err);
            };
        });
        
        User.remove({}, function(err) {
            User.create([
                { "username": "user1", "email": "user1@fake.dom", "password": "123456" },
                { "username": "user2", "email": "user2@fake.dom", "password": "654321" },
                { "username": "user3", "email": "user3@fake.dom", "password": "123456" }
            ], function(err, user1, user2, user3) {
                if (err) {
                    return done(err);
                };

                should.exist(user1);
                should.exist(user2);
                should.exist(user3);

                done();
            });
        });
    });

    after(function(done) {
        User.remove({}, function(err) {
            db.close();
            done();
        });
    });

    // describe("model", function() {
    //     it("should be return all (3) users list", function(done) {
    //         User.find(function(err, users) {
    //             if (err) {
    //                 return done(err);
    //             };
    //             should.exist(users);
    //             users.length.should.equal(3);
    //             done();
    //         });
    //     }); 

    //     it("should be return a user", function(done) {
    //         User.find({ "username": "user1" }, function(err, user) {
    //             if (err) {
    //                 return done(err);
    //             };
    //             should.exist(user);
    //             user.length.should.equal(1);
    //             user[0].email.should.equal("user1@fake.dom");
    //             done();
    //         });
    //     });

    //     it("should be failed to save because username already exist", function(done) {
    //         var newUser = new User({ "username": "user1", "email": "user1@domain.com", "password": "098765" });
    //         newUser.save(function(err, user, docAffected) {
    //             should.exist(err);
    //             err.code.should.equal(11000);
    //             should.not.exist(user);
    //             done();
    //         });
    //     });

    //     it("should failed to save because email already exist", function(done) {
    //         var newUser = new User({ "username": "user", "email": "user1@fake.dom", "password": "098765" });
    //         newUser.save(function(err, user, docAffected) {
    //             should.exist(err);
    //             should.not.exist(user);
    //             done();
    //         });
    //     });

    //     it("should be fail to validate because username is empty", function(done) {
    //         var newUser = new User({ "username": "", "email": "user1@domain.com", "password": "098765" });
    //         newUser.save(function(err, user, docAffected) {
    //             should.exist(err);                
    //             should.not.exist(user);
    //             done();
    //         });
    //     });

    //     it("should be fail to validate because username less than 3 letters", function(done) {
    //         var newUser = new User({ "username": "a", "email": "user1@fake.dom", "password": "098765" });
    //         newUser.save(function(err, user, docAffected) {
    //             should.exist(err);
    //             should.not.exist(user);
    //             done();
    //         });
    //     });

    //     it("should be fail to validate because username contains special character", function(done) {
    //         var newUser = new User({ "username": "user@$", "email": "user1@fake.dom", "password": "098765" });
    //         newUser.save(function(err, user, docAffected) {
    //             should.exist(err);
    //             should.not.exist(user);
    //             done();
    //         });
    //     });

    //     it("should be fail to validate because email is incorrect format", function(done) {
    //         var newUser = new User({ "username": "user1", "email": "user1", "password": "098765" });
    //         newUser.save(function(err, user, docAffected) {
    //             should.exist(err);
    //             should.not.exist(user);
    //             done();
    //         });
    //     });

    //     it("should be authenticated", function(done) {
    //         var isAuthenticated = User.login("user1", "123456", function(isAuthenticated) {
    //             isAuthenticated.should.equal(true);
    //             done();
    //         });
    //     });

    //     it("should be unauthenticated", function(done) {
    //         var isAuthenticated = User.login("user1", "xxxxxx", function(isAuthenticated) {
    //             isAuthenticated.should.equal(false);
    //             done();
    //         });
    //     });
        
    //     it("should be updating", function(done) {
    //         var newEmail = "modifiedEmail@fake.dom";
    //         User.findOne({ "username": "user3" }, function(err, user) {
    //             if (err) {
    //                 return done(err);
    //             };

    //             if (!user) {
    //                 return done(new Error("document not found"));
    //             };

    //             user.email = newEmail;
    //             user.increment();
    //             user.save(function(err, modifiedUser, docAffected) {
    //                 if (err) {
    //                     return done(err);
    //                 };
    //                 docAffected.should.equal(1);
    //                 modifiedUser.email.should.equal(newEmail);
    //                 done();
    //             });
    //         });
    //     });        

    //     it("should be deleting", function(done) {
    //         User.remove({ "username": "user3" }, function(err) {
    //             should.not.exist(err);
    //             done();
    //         });
    //     });
    // });

    describe("routing and controller", function() {
        describe("GET /users/:username", function() {
            it("should respond with related user json", function(done) {
                request(app)
                    .get('/users/user1')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .expect(function(res) {
                        res.body.should.not.equal({});
                    })
                    .end(done);
            });
        });

        

        describe('');
    });
});

// describe('GET /users/:username', function() {
//     it('respond with json', function(done) {
//         request(app)
//             .get('/users/user1')
//             .set('Accept', 'application/json')
//             .expect(200)
//             .expect({ message: 'details' })
//             .end(function(err, res) {  
//                 if (err) {
//                     return done(err);
//                 };

//                 done();
//             });
//     });
// });