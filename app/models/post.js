var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PostSchema = new Schema({
    title: { type: String, required: true },
    content: { type: String, required: true },
    tags: { type: [String] },
    createdDate: { type: Date, default: Date.now },
    lastModifiedDate: { type: Date, default: Date.now },
    _author: { type: Schema.Types.ObjectId, ref: 'users' },
    _comments: [{ type: Schema.Types.ObjectId, ref: 'comments' }]
});

var postModel = mongoose.model('posts', PostSchema);

module.exports = postModel;