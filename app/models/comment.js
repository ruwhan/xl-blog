var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CommentSchema = new Schema({
    title: { type: String, required: true },
    body: { type: String, required: true },
    createdDate: { type: Date, required: Date.now },
    lastModifiedDate: { type: Date, required: Date.now },
    _name: { type: Schema.Types.ObjectId, ref: 'user' },
    _posts: { type: Schema.Types.ObjectId, ref: 'post' }
});

CommentSchema.path('body').validate(function(value) {
    return value.length < 10 || value > 10000;
});

var commentModel = mongoose.model('comments', CommentSchema);
module.exports = commentModel;