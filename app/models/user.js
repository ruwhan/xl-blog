var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto');

var hashPassword = function(plainPassword, salt) {
    return crypto.pbkdf2Sync(plainPassword, salt, 1000, 128).toString('base64');
}

var generateAuthToken = function() {
    return crypto.randomBytes(128).toString('hex');
}

var UserSchema = new Schema({
    username: { type: String, default: '', unique: true },
    email: { type: String, default: '', unique: true },
    // authenticationToken: { type: String, default: "" },
    password: { type: String, default: '' },
    salt: { type: String, default: '' },
    createdDate: { type: Date, default: Date.now },
    lastModifiedDate: { type: Date, default: Date.now }
});

UserSchema.path('username').validate(function(value) {
    var me = this;

    if (value) {
        return !(/[!-.]|[:-@]|\/|\\|\^\|/gi.test(value)) && value.length >= 3;
    };

    return false;
}, 'Invalid username');

UserSchema.path('email').validate(function(value) {
    if (value) {
        return /\w+\.?\w+@\w+.\w+\.?\w+/gi.test(value);
    };

    return false;
}, 'Invalid email');

UserSchema.path('password').set(function(value) {
    console.log("set password");
    console.log(value);
    var me = this;
    me.salt = crypto.randomBytes(8).toString('base64');
    return hashPassword(value, me.salt);
});

// UserSchema.path('password').validate(function(value) {
//     return value.length >= 6 && value.length !== 0;
// }, 'minimum password length is 6');

UserSchema.static('validateCredentials', function(email, password, callback) {
    var me = this;
    me.findOne({ "email": email }, function(err, user) {
        if (err) {
            return callback(err, false);
        };

        if (!user) {
            return callback(null, false);
        };
        
        var isAuthenticated = user.password == hashPassword(password, user.salt);
        if (callback) {
            callback(null, isAuthenticated, user);
        };
    });
});

UserSchema.methods = {
    getAuthToken: function() {
        return generateAuthToken();
    }
};

// UserSchema.pre('save', function(next) {
//     var me = this;
//     me.authenticationToken = generateAuthToken();
//     next();
// });

var userModel = mongoose.model('users', UserSchema);

module.exports = exports = userModel;