var Repository = require('./repository').Repository;

var postsRepo = new Repository('post');

postsRepo.findOne = function(params, callback) {
    return postsRepo.Model.findOne(params)
    .populate('_author', 'username')
    // .populate('_comments')
    .exec(callback);
}

module.exports = postsRepo;