var postsRepository = require('../data/posts-repository');

module.exports.index = function(req, res, next) {
    postsRepository.findMany({ _author: req.user._id }, function(err, posts) {

        if (err) {
            return res.send(400, err);
        };

        if (!posts) {
            return res.send(404);
        };

        return res.json(posts);
    });
}

module.exports.details = function(req, res, next) {
    var id = req.params['id'];
    postsRepository.findOne({ _id: id }, function(err, post) {
        if (err) {
            return res.send(400, err);
        };

        if (!post) {
            return res.send(404);
        };

        return res.json(post);
    });
}

module.exports.create = function(req, res, next) {
    var newPost = req.body;
    postsRepository.save(newPost, function(err, post, docAffected) {
        if (err) {
            return res.send(400, err);
        };
        return res.json({
            model: post,
            affected: docAffected
        });
    });
}

module.exports.update = function(req, res, next) {
    var modifiedPost = req.body;

    postsRepository.update(modifiedPost, function(err, post, docAffected) {
        if (err) {
            return res.send(400, err);
        };

        return res.json({
            model: post,
            affected: docAffected
        });
    });
}

module.exports.remove = function(req, res, next) {
    var deletedPost = req.body;

    postsRepository.remove(deletedPost, function(err, post) {
        if (err) {
            return res.send(400, post);
        };

        return res.json({ model: post });
    });
}