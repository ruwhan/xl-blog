var passport = require('passport'),
    usersRepository = require('../data/users-repository');

module.exports.signin = function(req, res, next) {
    return res.send('signin');
}

module.exports.details = function(req, res, next) {
    usersRepository.findOne({ "username": req.params['username'] }, function(err, user) {


        if (err || !user) {
            return res.send(404, 'document/record not found');
        };

        return res.json(user);
    });
}

module.exports.create = function(req, res, next) {
    var newUser = req.body;
    usersRepository.save(newUser, function(err, user, docAffected) {
        if (err) {
            return res.send(err);
        };

        return res.json({ "model": user, "affected": docAffected });
    });
}

module.exports.update = function(req, res, next) {
    var modifiedUser = req.body;
    usersRepository.update(modifiedUser, function(err, user, docAffected) {
        if (err) {
            return res.send(err);
        };

        return res.json({ "model": user, "affected": docAffected });
    });
}

module.exports.remove = function(req, res, next) {
    var deletedUser = req.body;
    usersRepository.remove(deletedUser, function(err, user) {
        if (err) {
            return res.send(err);
        };

        return res.json({ "model": user });
    });
}

module.exports.authorized = function(req, res, next) {
    return res.json({ 
        _id: req.user._id,
        username: req.user.username, 
        email: req.user.email, 
        token: req.user.authenticationToken,
        createdDate: req.user.createdDate,
        lastModifiedDate: req.user.lastModifiedDate
    });
}

module.exports.unauthorized = function(req, res, next) {
    return res.send(401);
}