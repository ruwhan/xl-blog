define([
    'views/admin/adminLayout', 
    'models/post',
    'collections/posts',
    'views/posts/indexComView',
    'views/posts/freshView',
    'views/posts/editView',
    'views/posts/detailsView',
    'marionette'
], function(adminLayout, Post, Posts, IndexComView, FreshView, EditView, DetailsView) {
    var PostsController = Backbone.Marionette.Controller.extend({
        index: function() {
            var posts = new Posts();
            var indexCompositeView = new IndexComView({
                collection: posts
            });
            adminLayout.setContentRegionView(indexCompositeView);
        },

        fresh: function() {
            var newPost = new Post();
            var freshView = new FreshView({
                model: newPost
            });
            adminLayout.setContentRegionView(freshView);
        },

        edit: function(id) {
            var post = new Post({ _id: id });
            var editView = new EditView({
                model: post
            });
            adminLayout.setContentRegionView(editView);
        },

        details: function(id) {
            var post = new Post({ _id: id });
            var detailsView = new DetailsView({
                model: post
            });
            adminLayout.setContentRegionView(detailsView);
        },

        remove: function() {}
    });

    return PostsController;
});