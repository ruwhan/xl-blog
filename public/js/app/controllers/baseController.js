
define(['app/myApp', 'views/headerLayout', 'marionette'], function(myApp, HeaderLayout) {

    var BaseController = Backbone.Marionette.Controller.extend({
        initialize: function() {
            this.bindHeader();
        },
        bindHeader: function() {
            var headerLayout = new HeaderLayout();
            myApp.headerRegion.show(headerLayout);            
        }
    });

    return BaseController;
});