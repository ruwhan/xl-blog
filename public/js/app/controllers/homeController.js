define([
    'app/myApp', 
    'views/home/indexLayout',
    'views/home/aboutLayout',
    'controllers/baseController', 
    'marionette'
], function(myApp, IndexLayout, AboutLayout, BaseController) {
    var HomeController = BaseController.extend({
        index: function() {
            myApp.setMainRegionView(IndexLayout);
        },
        about: function() {
            myApp.setMainRegionView(AboutLayout);
        },
        blog: function() {
            
        }
    });

    return HomeController;
});