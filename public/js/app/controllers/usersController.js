define([
    'app/myApp', 
    'controllers/baseController', 
    'models/user',
    'models/credential',
    'views/users/signupView',
    'views/users/signinView',
    'marionette'
], function(myApp, BaseController, User, Credential, SignupView, SigninView) {
    var UsersController = BaseController.extend({
        fresh: function() {
            var user = new User();
            var signupView = new SignupView({
                model: user
            });
            myApp.setMainRegionView(signupView);
        },
        edit: function() {

        },
        details: function() {

        },
        remove: function() {

        },
        signin: function() {
            var cred = new Credential();
            var signinView = new SigninView({
                model: cred
            });
            myApp.setMainRegionView(signinView);
        }
    });

    return UsersController;
});