define([
    'app/myApp',
    'controllers/baseController', 
    'views/admin/adminLayout',
    'views/admin/sidebarMenuView',
    'views/admin/dashboard'
], function(myApp, BaseController, adminLayout, SidebarMenuView, Dashboard) {
    var AdminController = BaseController.extend({
        index: function() {
            var sidebarMenuView = new SidebarMenuView();
            var dashboard = new Dashboard();

            myApp.mainRegion.show(adminLayout);
            adminLayout.sidebarRegion.show(sidebarMenuView);
            adminLayout.contentRegion.show(dashboard);
        }
    });

    return AdminController;
});