define([
    'controllers/homeController',
    'controllers/adminController',
    'controllers/usersController',
    'controllers/postsController',
    'marionette'
], function(HomeController, AdminController, UsersController, PostsController) {
    var Router = Backbone.Marionette.AppRouter.extend({});    
    var router = new Router();

    var homeController = new HomeController();
    var adminController = new AdminController();
    var usersController = new UsersController();
    var postsController = new PostsController();

    // home routes
    router.processAppRoutes(homeController, {
        "/": "index",
        "blog": "blog",
        "about": "about"
    });

    // admin routes
    router.processAppRoutes(adminController, {
        "admin/dashboard/": "index"
    });

    // users routes
    router.processAppRoutes(usersController, {
        "signup": "fresh",
        "signin": "signin",
        "admin/users/edit/": "edit",
        "admin/users/details/:username": "details",
        "admin/users/remove/": "remove",
    });

    // posts routes
    router.processAppRoutes(postsController, {
        "admin/posts/": "index",
        "admin/posts/new/": "fresh",
        "admin/posts/edit/:id": "edit",
        "admin/posts/details/:id": "details",
        "admin/posts/remove/": "remove"
    });

    return router;

    // var routers = {
    //     homeRouter: function() {
    //         var controller = new HomeController();
    //         var Router = Backbone.Marionette.AppRouter.extend({
    //             controller: controller,
    //             appRoutes: {
    //                 "/": "index",
    //                 "about": "about"
    //             }
    //         });
    //         return new Router;
    //     }
    // };
    // var routes = function() {
    //     for (key in routers) {
    //         routers[key]();
    //     };
    // };

    // return routes;
});