define(['marionette'], function(Marionette) {
    var myApp = new Backbone.Marionette.Application();

    myApp.setMainRegionView = function(view) {
        this.mainRegion.close();
        this.mainRegion.show(view);
    };

    myApp.addRegions({
        headerRegion: "#header",
        mainRegion: "#main",
        footerRegion: "#footer",
        modalRegion: "#modal"
    });

    myApp.addInitializer(function() {
        console.log('init application');
        Backbone.history.navigate('/', { trigger: true });
    });

    return myApp;
});