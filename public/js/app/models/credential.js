define(['jquery', 'backbone'], function() {
    var Credential = Backbone.Model.extend({ 
        defaults: {
            email:"",
            password: "",
        },
        signin: function(options) {
            var me = this;
            var jqXhr = $.ajax({
                url: "/signin",
                type: "POST",
                data: me.attributes
            });

            jqXhr.done(function(data, textStatus, jqXHR) {
                var user = data;
                me.trigger('authentication:success', { user: user });
            });

            jqXhr.fail(function(jqXHR, textStatus, errorThrown) {
                me.trigger('authentication:failed');
            });
        }
    });

    return Credential;
});