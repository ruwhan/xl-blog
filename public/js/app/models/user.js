define(['jquery', 'backbone'], function() {
    var User = Backbone.Model.extend({
        defaults: {
            "_id": undefined,
            "username": '',
            "email": '',
            "password": '',
            "passwordConfirmation": '',
            "rememberMe": false
        },
        idAttributes: '_id',
        urlRoot: '/users',
        validate: function(attributes, opt) {
            var me = this;
            var errors = {};
            if (attributes.password !== attributes.passwordConfirmation) {                

                if (!errors['password']) {
                    errors['password'] = {};
                };

                if (!errors['password'].messages) {
                    errors['password'].messages = [];
                };
                
                errors['password'].messages.push("password confirmation doesn't match");
            };

            if (!$.isEmptyObject(errors)) {
                return errors;
            };
        }
    });

    return User;
});