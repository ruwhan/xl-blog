define(['backbone'], function() {
    var Post = Backbone.Model.extend({
        idAttribute: '_id',
        defaults: {
            title: "",
            content: "",
            tags: [],
            createdDate: Date.now,
            lastModifiedDate: Date.now,
            _author: null
            // _comments: []
        },
        urlRoot: '/posts'
    });

    return Post;
});