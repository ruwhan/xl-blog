define([
    'text!templates/admin/adminLayout.hbs', 
    'marionette', 
    'handlebars'
], function(template) {
    var AdminLayout = Backbone.Marionette.Layout.extend({
        className: 'row',
        template: Handlebars.compile(template),
        regions: {
            sidebarRegion: "#sidebar",
            contentRegion: "#content"
        },
        onRender: function() {
            return this;
        },
        
        setContentRegionView: function(view) {
            var me = this;
            me.contentRegion.close();
            me.contentRegion.show(view);
        }
    });

    var adminLayout = new AdminLayout();

    // singleton
    return adminLayout;
});