define(['text!templates/admin/dashboard.hbs', 'marionette', 'handlebars'], function(template) {
    var Dashboard = Backbone.Marionette.Layout.extend({
        template: Handlebars.compile(template)
    });

    return Dashboard;
});