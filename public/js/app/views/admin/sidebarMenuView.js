define([
    'views/admin/dashboard',
    'text!templates/admin/sidebarMenu.hbs', 
    'marionette', 
    'handlebars'
], function(Dashboard, template) {
    var SidebarMenuView = Backbone.Marionette.ItemView.extend({
        tagName: 'ul',
        className: 'nav nav-pills nav-stacked',
        template: Handlebars.compile(template),
        events: {
            "click #nav-dashboard": "onNavDashboardClicked",
            "click #nav-posts": "onNavPostsClicked",
            "click #nav-stat": "onNavStatClicked",
            "click #nav-activity": "onNavActivityClicked",
            "click #nav-account": "onNavAccountClicked"
        },
        onNavDashboardClicked: function(e) {
            e.preventDefault();
            Backbone.history.navigate('admin/dashboard/', { trigger: true });
        },
        onNavPostsClicked: function(e) {
            e.preventDefault();
            Backbone.history.navigate('admin/posts/', { trigger: true });
        },
        onNavStatClicked: function(e) {
            e.preventDefault();
            Backbone.history.navigate('admin/statistics/', { trigger: true });
        },
        onNavActivityClicked: function(e) {
            e.preventDefault();
            Backbone.history.navigate('admin/activity/', { trigger: true });
        },
        onNavAccountClicked: function() {
            Backbone.history.navigate('admin/users/edit');
            e.preventDefault();
        }
    });

    return SidebarMenuView;
});