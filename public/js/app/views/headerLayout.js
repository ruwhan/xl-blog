define(['handlebars', 'text!templates/headerLayout.hbs', 'marionette'], function(Handlebars, template) {
    var HeaderLayout = Backbone.Marionette.Layout.extend({
        initialize: function() {
            var me = this;
            return me;
        },
        className: 'row',
        template: Handlebars.compile(template),
        events: {
            "click #home": "onClickHomeLink",
            "click #blog": "onClickBlogLink",
            "click #about": "onClickAboutLink",
            "click #signin": "onClickSigninLink",
            "click #signup": "onClickSignupLink"
        },
        onClickHomeLink: function(e) {
            e.preventDefault();
            Backbone.history.navigate('/', { trigger: true });
        },
        onClickBlogLink: function(e) {
            e.preventDefault();
            Backbone.history.navigate('blog', { trigger: true });
        },
        onClickAboutLink: function(e) {
            e.preventDefault();
            Backbone.history.navigate('about', { trigger: true });
        },
        onClickSigninLink: function(e) {
            e.preventDefault();
            Backbone.history.navigate('signin', { trigger: true });
        },
        onClickSignupLink: function(e) {
            e.preventDefault();
            Backbone.history.navigate('signup', { trigger: true });
        }
    });

    return HeaderLayout;
});