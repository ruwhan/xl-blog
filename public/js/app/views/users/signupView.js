define(['text!templates/users/signup.hbs', 'handlebars', 'marionette'], function(template) {
    var SignupView = Backbone.Marionette.ItemView.extend({
        initialize: function() {
            var me = this;
            me.listenTo(me.model, 'invalid', function(model, error) {
                console.log("invalid");
                console.log(error);
            });
        },
        className: 'row',
        template: Handlebars.compile(template),
        ui: {
            inputUsername: "#input-username",
            inputEmail: "#input-email",
            inputPassword: "#input-password",
            inputPasswordConf: "#input-password-conf"
        },
        events: {
            "submit #signup-form": "onSubmit"
        },
        onSubmit: function(e) {
            e.preventDefault();

            var me = this;
            var username = me.ui.inputUsername.val();
            var email = me.ui.inputEmail.val();
            var password = me.ui.inputPassword.val();
            var passwordConfirmation = me.ui.inputPasswordConf.val();
            me.model.save({
                username: username,
                email: email,
                password: password, 
                passwordConfirmation: passwordConfirmation
            }, {
                success: function(model, response, options) {
                    Backbone.history.navigate("signin");
                },
                error: function(model, response, options) {
                    console.log(response);
                }
            });
        }
    });

    return SignupView;
});