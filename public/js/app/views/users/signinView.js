define([
    'app/myApp',
    'text!templates/users/signin.hbs',
    'handlebars',
    'marionette'
], function(myApp, template) {
    var SigninView = Backbone.Marionette.ItemView.extend({
        initialize: function() {
            var me = this;
            
            me.listenTo(me.model, 'authentication:success', function(response) {
                myApp.user = new Backbone.Model(response.user);
                Backbone.history.navigate('admin/dashboard/', { trigger: true });
            });

            me.listenTo(me.model, 'authentication:failed', function() {
                Backbone.history.navigate('signin', { trigger: true });
            });

            return me;
        },
        onRender: function() {
            var me = this;
            // me.$('signin-form')[0].reset();
            return me;
        },
        className: 'row',
        template: Handlebars.compile(template),
        ui: {
            inputEmail: "#input-email",
            inputPassword: "#input-password"
        },
        events: {
            "submit #signin-form": "onSubmit"
        },
        onSubmit: function(e) {
            e.preventDefault();
            var me = this;
            var emailCredential = me.ui.inputEmail.val();
            var passwordCredential = me.ui.inputPassword.val();
            me.model.set({ email: emailCredential, password: passwordCredential });
            me.model.signin();
        }
    });

    return SigninView;
});