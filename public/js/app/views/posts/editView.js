define([
    'views/posts/formView'
], function(FormView) {
    var EditView = FormView.extend({
        afterInitialize: function() {
            var me = this;
            me.model.fetch({
                success: function(model, response, options) {
                    me.render();
                },
                error: function(model, response, options) {
                    console.log(response);
                }
            });
        }
    });

    return EditView;
});