define(['text!templates/posts/indexView.hbs', 'marionette', 'handlebars'], function(template) {
    var IndexView = Backbone.Marionette.ItemView.extend({
        tagName: 'tr',
        template: Handlebars.compile(template),
        events: {
            'click #details': "onDetailsClicked",
            'click #edit': 'onEditClicked',
            'click #remove': 'onRemoveClicked'
        },
        onDetailsClicked: function(e) {
            e.preventDefault();
            
            var me = this;
            var id = me.model.get('_id');
            Backbone.history.navigate('admin/posts/details/' + id, { trigger: true });
        },
        onEditClicked: function(e) {
            e.preventDefault();

            var me = this;
            var id = me.model.get('_id');
            Backbone.history.navigate('admin/posts/edit/' + id, { trigger: true });
        },
        onRemoveClicked: function(e) {
            e.preventDefault();
            // console.log('onRemoveClicked');
        }
    });

    return IndexView;
});