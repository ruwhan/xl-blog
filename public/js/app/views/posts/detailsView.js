define([
    'text!templates/posts/detailsView.hbs',
    'marionette',
    'handlebars'
], function(template) {
    var DetailsView = Backbone.Marionette.ItemView.extend({
        initialize: function() {
            var me = this;

            me.model.fetch({
                success: function(model, response, options) {
                    me.render();
                },
                error: function(model, response, options) {
                    console.log(response);
                }
            });
            return me;
        },
        template: Handlebars.compile(template)
    });

    return DetailsView;
});