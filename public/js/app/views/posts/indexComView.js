define([ 
    'views/posts/indexView', 
    'text!templates/posts/indexComView.hbs',
    'marionette', 
    'handlebars'
], function(IndexView, template) {
    var IndexComView = Backbone.Marionette.CompositeView.extend({
        // tag: 'table',
        // className: 'table table-hover',
        itemView: IndexView,
        template: Handlebars.compile(template),
        itemViewContainer: '#posts',
        initialize: function() {
            var me = this;
            me.collection.fetch();
            return me;
        },
        events: {
            "click #create-post": "onClicked"
        },
        onClicked: function(e) {
            e.preventDefault();
            Backbone.history.navigate("admin/posts/new/", { trigger: true });
        }
    });

    return IndexComView;
});