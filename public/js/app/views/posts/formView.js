define([
    'app/myApp',
    'text!templates/posts/formView.hbs',
    'marionette',
    'handlebars'
], function(myApp, template) {
    var FormView = Backbone.Marionette.ItemView.extend({
        initialize: function() {
            var me = this;
            if (me.afterInitialize) {
                me.afterInitialize();
            };
            console.log(me.model);
            return me;
        },
        template: Handlebars.compile(template),
        ui: {
            inputTitle: '#inputTitle', 
            taContent: '#taContent',
            inputTags: '#inputTags'
        },
        events: {
            "submit #form": "onSubmit"
        },
        onSubmit: function(e) {
            e.preventDefault();

            var me = this;
            
            var title = me.ui.inputTitle.val();
            var content = me.ui.taContent.val();
            var tags = me.ui.inputTags.val().split(',');
            var lastModifiedDate = me.model.isNew() ? undefined : Date.now;
            var author_id = myApp.user.get('_id');

            for (var i = 0; i < tags.length; i++) {
                tags[i] = tags[i].trim();
            };

            me.model.save({
                title: title,
                content: content,
                tags: tags,
                lastModifiedDate: lastModifiedDate,
                _author: author_id
            }, {
                success: function(model, response, options) {
                    Backbone.history.navigate("admin/posts/", { trigger: true });
                },
                error: function(model, response, options) {
                    console.log(response);
                }
            });
        }
    });

    return FormView;
});