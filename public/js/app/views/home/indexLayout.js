define(['text!templates/home/index.hbs', 'marionette', 'handlebars'], function(template) {
    var IndexLayout = Backbone.Marionette.Layout.extend({
        className: 'row',
        template: Handlebars.compile(template)
    });

    return IndexLayout;
});