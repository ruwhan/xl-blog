define(['text!templates/home/about.hbs', 'marionette', 'handlebars'], function(template) {
    var AboutLayout = Backbone.Marionette.Layout.extend({
        className: 'row',
        template: Handlebars.compile(template)
    });

    return AboutLayout;
});