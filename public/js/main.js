require.config({
    "baseUrl": "../..",
    "paths": {
        // libraries
        "jquery": "bower_components/jquery/dist/jquery.min",
        "underscore": "bower_components/underscore/underscore",
        "backbone": "bower_components/backbone/backbone",
        "wreqr": "bower_components/backbone.wreqr/lib/backbone.wreqr.min",
        "marionette": "bower_components/marionette/lib/backbone.marionette.min",
        "handlebars": "bower_components/handlebars/handlebars.min",
        "bootstrap": "bower_components/bootstrap/dist/js/bootstrap.min",

        // Requirejs plugins
        "text": "bower_components/requirejs-text/text",

        // application
        "app": "js/app",
        "models": "js/app/models",
        "collections": "js/app/collections",
        "templates": "js/app/templates",
        "views": "js/app/views",
        "controllers": "js/app/controllers"
    },
    shim: {
        "jquery": {
            exports: '$'
        },
        "underscore": {
            exports: '_'
        },
        "backbone": {
            deps: [ 'underscore', 'jquery' ],
            exports: 'Backbone'
        },
        "marionette": {
            deps: [ 'backbone', 'wreqr' ],
            exports: 'Backbone.Marionette'
        },
        "handlebars": {
            exports: 'Handlebars'
        },
        "bootstrap": {
            deps: ['jquery']
        }
    }
});

requirejs(['app/myApp', 'app/router', 'jquery', 'bootstrap', 'backbone'], function(myApp, router) {

    router;
    if (!Backbone.history.started) {
        Backbone.history.start();  
    };
    myApp.start();
});