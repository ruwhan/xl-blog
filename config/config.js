var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'try-express-marionette-full-app'
    },
    port: 3000,
    db: 'mongodb://localhost/try-express-marionette-full-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'try-express-marionette-full-app'
    },
    port: 3000,
    db: 'mongodb://localhost/try-express-marionette-full-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'try-express-marionette-full-app'
    },
    port: 3000,
    db: 'mongodb://localhost/try-express-marionette-full-production'
  }
};

console.log('loading environment config ' + env);
module.exports = config[env];
