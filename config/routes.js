
module.exports = function(app, passport){
    var home = require('../app/controllers/home');
    var users = require('../app/controllers/users');
    var posts = require('../app/controllers/posts');
    var comments = require('../app/controllers/comments');

	// home route
	app.get('/', home.index);

    // users route
    app.get('/authorized', users.authorized);
    app.get('/unauthorized', users.unauthorized);
    app.post('/signin', 
        passport.authenticate('local', { successRedirect: "/authorized", failureRedirect: "/unauthorized" })
    );
    app.get('/users/:username', users.details);
    app.post('/users', users.create);
    app.put('/users', users.update);
    app.del('/users', users.remove);

    app.get('/posts', posts.index);
    app.get('/posts/:id', posts.details);
    app.post('/posts', posts.create);
    app.put('/posts', posts.update);
    app.put('/posts/:id', posts.update);
    app.del('/posts', posts.remove);

    app.post('/comments', comments.create);
    app.del('/comments', comments.remove);
};
