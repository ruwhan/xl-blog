var User = require('../app/models/user'),
    // passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;

var passportConfig =  function(passport) {
    passport.use(new LocalStrategy(
        {
            usernameField: "email",
            passwordField: "password"
            // session: false
        },
        function(email, password, done) {
            User.validateCredentials(email, password, function(err, isValid, user) {
                
                if (err) {
                    return done(err, false);
                };

                if (!user) {
                    return (null, false, { "message": "Username or email not found" });
                };

                if (!isValid) {
                    return done(null, false, { "message": "Incorrect password" });
                };
                
                
                return done(null, user);
            });
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
};

module.exports = passportConfig;